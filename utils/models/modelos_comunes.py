# MODELOS COMUNES

from django.db import models

class CamposComunes(models.Model):
  """ Campos comunes para todos los modelos """

  status = models.BooleanField(default=True)
  fecha_de_creacion = models.DateTimeField(auto_now_add=True) # auto_now_add=True: se guarda la fecha de creacion al momento de crear el registro
  ultima_actualizacion = models.DateTimeField(auto_now=True) # auto_now=True: se guarda la fecha al momento de realizar cambios en un registro existente

  class Meta:
    abstract = True