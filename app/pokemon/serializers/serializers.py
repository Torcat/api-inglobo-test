import time
from rest_framework import serializers

# Se importan los modelos necesarios
from app.pokemon.models import Pokemon, Evoluciones

class EvolucionesSerializers(serializers.ModelSerializer):
  """ Evoluciones serializer """
  class Meta:
    """ Class meta """
    model = Evoluciones
    fields = ('nombre',)

class PokemonSerializers(serializers.ModelSerializer):
  """ Serializer de Pokemon """
  evoluciones = EvolucionesSerializers(many=True, read_only=True)
  efectividad = serializers.SerializerMethodField('get_efectividad')
  class Meta:
    """ Class meta """
    model = Pokemon
    fields = (
      'id',
      'nombre',
      'tipo',
      'poder_ataque',
      'evoluciones',
      'efectividad'
    )
  
  def get_efectividad(self, obj):
    """ Definiendo efectividad """
    valores = {
      'fuego': obj.poder_ataque * 1.5,
      'agua': obj.poder_ataque * 0.5,
      'viento': obj.poder_ataque * 2.0,
      'tierra': obj.poder_ataque * 1.0,
    }
    return valores

class NuevoPokemonSerializer(serializers.Serializer):
  """ Serializer """
  # PUEDO DEFINIR CUALQUIER PARAMETRO PARA EL SERIALIZER
  nombre = serializers.CharField(
    max_length=100,
    allow_blank = False,
    allow_null = False,
    required = True
  )
  tipo = serializers.CharField(
    max_length=100,
    allow_blank = False,
    allow_null = False,
    required = True
  )
  poder_ataque = serializers.IntegerField(
    allow_null = False,
    required = True
  )

  def validate(self, data):
    """ Validar datos (REGLAS DE NEGOCIO) """
    if data['nombre'].lower() == 'newtwo':
      raise serializers.ValidationError({"error": "Pokemon maligno!"})
    # LO QUE SE RETORNE ACA LO RECIBE EL DATA DEL CREATE Y UPDATE
    return data

  def create(self, data):
    """ Crear un valor """
    pokemon = Pokemon.objects.create(
      nombre = data['nombre'],
      tipo = data['tipo'],
      poder_ataque = data['poder_ataque']
    )
    return data

class AddEvolucionSerializer(serializers.Serializer):
  """ Agregar una evolución a un pokemon """
  nombre = serializers.CharField(
    max_length=100,
    allow_blank = False,
    allow_null = False,
    required = True
  )
  
  id_pokemon = serializers.IntegerField(
    allow_null = False,
    required = True
  )

  def validate(self, data):
    """ Validar los datos """
    try:
      pokemon = Pokemon.objects.get(id=data['id_pokemon'])
      query = Evoluciones.objects.filter(
        nombre__iexact = data['nombre'],
        pokemon = pokemon
      )
      if query.exists():
        raise serializers.ValidationError(
          {"error": "Ya el pokemon tiene esa evolución!"}
        )
    except Pokemon.DoesNotExist:
      raise serializers.ValidationError(
        {"error": "Pokemon no existe!"}
      )
    return data

  def create(self, data):
    """ Crear una evolución """
    Evoluciones.objects.create(
      nombre = data['nombre'],
      pokemon = Pokemon.objects.get(id=data['id_pokemon'])
    )
    return data

class EnviandoPokemonSerializer(serializers.Serializer):
  """ Enviar pokemon por email """
  id_pokemon = serializers.IntegerField(
    allow_null = False,
    required = True
  )

  def validate(self, data):
    """ Validar los datos """
    try:
      pokemon = Pokemon.objects.get(id=data['id_pokemon'])
    except Pokemon.DoesNotExist:
      raise serializers.ValidationError(
        {"error": "Pokemon no existe!"}
      )
    self.context['pokemon'] = pokemon
    return data

  def create(self, data):
    """ Enviar por email """
    nombre_pokemon = self.context['pokemon'].nombre
    for i in range(0, 5):
      print(f"Enviando pokemon {nombre_pokemon}...")
      time.sleep(1)
    print(f"Enviado!")
    return data