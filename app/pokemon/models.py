from django.db import models

# Create your models here.

class Pokemon(models.Model):
  """ Modelo de Pokemon """
  # SIMPRE SE CREA UN CAMPO ID Y ES EL PRIMARY KEY (PK)
  nombre = models.CharField(max_length=100)
  tipo = models.CharField(max_length=100)
  poder_ataque = models.IntegerField()

  def __str__(self):
    return self.nombre

  class Meta:
    """ Class meta """
    db_table = 'pokemon'

class Evoluciones(models.Model):
  """ Evoluciones del pokemon """
  nombre = models.CharField(max_length=100)
  pokemon = models.ForeignKey(
    Pokemon,
    related_name="evoluciones",
    on_delete=models.CASCADE
  )

  def __str__(self):
    return self.nombre

  class Meta:
    db_table = 'evolucion'