from django.apps import AppConfig


class PokemonConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    # Se renombra la app por haberla ingresado dentro de la carpeta app
    name = 'app.pokemon'
