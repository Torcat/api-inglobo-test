# DECORADOR (ESTABLECE EL TIPO DE PETICIÓN)
from rest_framework.decorators import api_view

# ENVIA LA RESPUESTA
from rest_framework.response import Response

# DEFINE EL STATUS DE LA RESPUESTA
from rest_framework import status

# SERIALIZERS
from app.pokemon.serializers.serializers import (
  NuevoPokemonSerializer,
  PokemonSerializers,
  AddEvolucionSerializer,
  EnviandoPokemonSerializer
)

# MODELOS
from app.pokemon.models import Pokemon

@api_view(['GET', 'POST'])
def generic_view(request):
  """ Se recibe la solicitud, ya sea get o post """
  # Si se recibe solicitud del tipo GET
  if request.method == 'GET':
    # Se consultan los datos
    queryset = Pokemon.objects.all()

    # Se hace la serializacion 
    serializer = PokemonSerializers(queryset, many=True)

    # Se devielve la respuesta
    return Response(data = serializer.data, status=status.HTTP_200_OK)
  
  # Si se recibe solicitud del tipo POST
  if request.method == 'POST':
    serializer = NuevoPokemonSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    nuevo_modelo = serializer.save()
    return Response(data = nuevo_modelo, status=status.HTTP_201_CREATED)

@api_view(['GET', 'PUT', 'DELETE'])
def pokemon_detail(request, pk):
  """ URL detallada """
  try:
    pokemon = Pokemon.objects.get(id=pk) #pk es el id
  except Pokemon.DoesNotExist:
    response = {
      'error': 'El pokemon no existe'
    }
    return Response(data= response, status=status.HTTP_404_NOT_FOUND)

  if request.method == 'GET':
    serializer = PokemonSerializers(pokemon)
    return Response(serializer.data, status=status.HTTP_200_OK)

  if request.method == 'PUT':
    serializer = PokemonSerializers(pokemon, data=request.data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return Response(serializer.data, status=status.HTTP_200_OK)

  if request.method == 'DELETE':
    pokemon.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def add_evolucion(request):
  """ Agregar una evolución """
  if request.method == 'POST':
    serializer = AddEvolucionSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return Response(status=status.HTTP_201_CREATED)

@api_view(['POST'])
def send_by_email(request):
  """ Enviando por email """
  if request.method == 'POST':
    serializer = EnviandoPokemonSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    message = {
      'message': 'El pokemon se ha enviado por email'
    }
    return Response(data=message, status=status.HTTP_200_OK)