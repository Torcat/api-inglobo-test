from django.urls import path

# Se importan las vistas
from .views import generic_view, pokemon_detail, add_evolucion, send_by_email

# Se genera la url para acceder
urlpatterns = [
    path('pokemon/', generic_view),
    path('pokemon/<int:pk>/', pokemon_detail),
    path('pokemon/add-evolucion/', add_evolucion),
    path('pokemon/send/', send_by_email),
]