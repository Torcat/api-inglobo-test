from django.db import models

class ModeloDigimon(models.Model):
  """ Modelo pokemon """
  nombre = models.CharField(max_length=100)
  raza = models.CharField(max_length=100)

  def __str__(self):
    return self.nombre

  class Meta:
    """ Class meta """
    db_table = 'modelo_digimon'