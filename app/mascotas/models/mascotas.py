# MODELO MASCOTAS
from django.db import models

# Tipo de mascota
from app.mascotas.models.tipo_de_mascota import TipoDeMascota

# CAMPOS COMUNES
from utils.models.modelos_comunes import CamposComunes

class Mascotas(CamposComunes):
  """ Modelo Mascotas """
  nombre = models.CharField(max_length=50)
  codigo = models.BigIntegerField()
  edad = models.IntegerField()
  fecha_de_nacimiento = models.DateField()
  tipo_de_mascota = models.ForeignKey(
    TipoDeMascota,
    on_delete=models.CASCADE
  )

  def __str__(self):
    return self.nombre

  class Meta:
    db_table = 'mascotas'