# TIPOS DE MASCOTAS

from django.db import models

# CAMPOS COMUNES
from utils.models.modelos_comunes import CamposComunes

class TipoDeMascota(CamposComunes):
  """ Tipo de mascota modelo """
  nombre = models.CharField(max_length=50)

  def __str__(self):
    return f"{self.nombre}"

  class Meta:
    db_table = 'tipo_de_mascota'
    # ordering = ['-id'] # Ordena los registros por id de forma descendente similar a TipoDeMascota.objects.all().order_by('-id')