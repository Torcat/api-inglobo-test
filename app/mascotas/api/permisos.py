from rest_framework.permissions import BasePermission

class EsAdministrador(BasePermission):
  """ Permiso personalizado para administrador """
  def has_permission(self, request, view):
    # request.user obtiene el modelo del usuario logueado
    # flag = bool(request.user.tipo_de_usuario == 'ADMIN')
    # view: La variable puede acceder a cualquier atributos y metodos de la clase GenericViewSet
    return request.user.tipo_de_usuario == 'ADMIN'

class PermisoLoco(BasePermission):
  """ Prueba para has_object_permission """
  def has_object_permission(self, request, view, obj):
    # obj: El objeto que se esta consultando
    if request.user.tipo_de_usuario == 'INVITADO' and obj.nombre == 'Gato':
      return True
    return False
  # return request.user.tipo_de_usuario == 'INVITADO' and obj.nombre == 'Gato'