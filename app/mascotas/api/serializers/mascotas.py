# MODELO
from app.mascotas.models.mascotas import Mascotas
from app.mascotas.models.tipo_de_mascota import TipoDeMascota

# SERIALIZERS
from rest_framework import serializers

from app.mascotas.api.serializers.tipo_de_mascotas import TipoDeMascotaModelSerializer

class MascotasModelSerializer(serializers.ModelSerializer):
  """ Tipo de mascota model serialzier """
  # serializers.StringRelatedField Retorna el valor del metodo __str__ de la clase del modelo
  # tipo_de_mascota = serializers.StringRelatedField(read_only = True)
  tipo_de_mascota = TipoDeMascotaModelSerializer(read_only = True)
  class Meta:
    model = Mascotas
    fields = (
      'id',
      'nombre',
      'codigo',
      'edad',
      'fecha_de_nacimiento',
      'tipo_de_mascota',
      'status',
      'fecha_de_creacion',
      'ultima_actualizacion'
    )

class NuevaMascotaSerializer(serializers.Serializer):
  """ nueva mascota serializer """
  nombre = serializers.CharField(max_length=50, required = True)
  codigo = serializers.IntegerField(required = True)
  edad = serializers.IntegerField(required = True)
  fecha_de_nacimiento = serializers.DateField(required = True)
  tipo_de_mascota = serializers.IntegerField(required = True)

  def validate(self, data):
    try:
      tipo_de_mascota = TipoDeMascota.objects.get(
        id=data['tipo_de_mascota']
      )
      self.context['tipo_de_mascota'] = tipo_de_mascota
    except TipoDeMascota.DoesNotExist:
      raise serializers.ValidationError({"error": "Tipo de mascota no existe"})
    return data

  def create(self, data):
    """ crear nueva mascota """
    # tipo_de_mascota = self.context['tipo_de_mascota']
    del data['tipo_de_mascota']

    # AL ELIMINAR tipo_de_mascota data queda con estos campos:
    # nombre = data['nombre']
    # codigo = data['codigo']
    # edad = data['edad']
    # fecha_de_nacimiento = data['fecha_de_nacimiento']

    nueva_mascota = Mascotas.objects.create(
      **data,
      tipo_de_mascota=self.context['tipo_de_mascota']
      )
    return data