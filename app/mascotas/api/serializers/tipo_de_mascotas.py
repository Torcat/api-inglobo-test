# MODELO
from app.mascotas.models.tipo_de_mascota import TipoDeMascota

# SERIALIZERS
from rest_framework import serializers

class TipoDeMascotaModelSerializer(serializers.ModelSerializer):
  """ Tipo de mascota model serialzier """
  class Meta:
    model = TipoDeMascota
    fields = '__all__'