# VIEWS MASCOTAS

from rest_framework import (
  viewsets,
  mixins,
  status
)

from rest_framework.response import Response

# PERMISSIONS
from rest_framework.permissions import (
  AllowAny,
  IsAuthenticated
)

from app.mascotas.api.permisos import (
  EsAdministrador,
  PermisoLoco
)

# MODELOS
from app.mascotas.models.mascotas import Mascotas

# SERIALIZERS
from app.mascotas.api.serializers.mascotas import (
  MascotasModelSerializer,
  NuevaMascotaSerializer
)

from rest_framework.decorators import action

class MascotasViewSet(mixins.ListModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      viewsets.GenericViewSet):

  """ Mascotas view set """
  serializer_class = MascotasModelSerializer
  # LA VARIABLE 'queryset' DEFINE UNA CONSULTA SENCILLA
  # queryset = Mascotas.objects.filter(status=True)
  
  # lookup_field: es el campo que se usa para identificar un objeto
  # Ejemplo: en el endpoint /api/mascotas/1/ se usa el campo pk (id)

  # lookup_field = 'id'

  # list: GET -> TODOS LOS REGISTROS
  # retrieve: GET -> UN REGISTRO ESPECIFICO (lookup_field)
  # update: PUT -> ACTUALIZAR UN REGISTRO
  # destroy: DELETE -> ELIMINAR UN REGISTRO

  # SE PUEDEN EDITAR POR SEPARADO
  def get_permissions(self):
    permissions = []
    if self.action in ['nueva_mascota', 'list']:
      permissions = [IsAuthenticated]
    elif self.action in ['destroy', 'update', 'partial_update']:
      permissions = [EsAdministrador]
    elif self.action in ['retrieve']:
      permissions = [PermisoLoco]
    else:
      permissions = [AllowAny]
    return [p() for p in permissions]

  # EL METODO get_queryset() DEVUELVE UNA CONSULTA COMPLEJA
  def get_queryset(self):
    queryset = Mascotas.objects.filter(status=True)
    return queryset
  
  # ESTABLECE EL CAMPO EN EL CUAL SE REALIZARÁN LAS PETICIONES INDIVIDUALES
  # lookup_field = 'nombre'

  # url_pah indica el valor despues del slash "/"
  @action(detail=False, url_path='nueva', methods=['POST'])
  def nueva_mascota(self, request):
    """ crear ueva mascota """
    serializer = NuevaMascotaSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    valor = serializer.save()
    return Response(data=valor, status=status.HTTP_201_CREATED)

  
  def destroy(self, request, *args, **kwargs):
    """ eliminar una mascota """
    # get_object() devuelve el objeto que se va a eliminar
    # Ejemplo: es similar a Mascota.objects.get(lookup_field=pk)
    instance = self.get_object()
    self.perform_destroy(instance)
    response = {
      "mensaje": "Mascota eliminada"
    }
    return Response(response, status=status.HTTP_200_OK)

  # EL METODO perform_destroy() ES EL QUE SE EJECUTA AL ENVIAR UN DELETE
  def perform_destroy(self, instance):
    instance.status = False
    instance.save()