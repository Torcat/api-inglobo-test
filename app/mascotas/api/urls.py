# Django urls
from django.urls import (
  path,
  include
)

from rest_framework.routers import DefaultRouter

from app.mascotas.api.views.mascotas import MascotasViewSet


router = DefaultRouter()
router.register(r'mascotas', MascotasViewSet, basename='mascotas')

urlpatterns = [
  path('', include(router.urls))
]