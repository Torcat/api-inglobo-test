from rest_framework import (
  mixins,
  viewsets,
  status
)

from rest_framework.response import Response

from app.usuarios.models.usuarios import Usuario

from app.usuarios.api.serializers.usuarios import *

from rest_framework.decorators import action

from rest_framework.permissions import (IsAuthenticated)

from app.usuarios.api.permisos import EsElMismoUsuario

class UsuariosViewSet(viewsets.GenericViewSet,
                      mixins.RetrieveModelMixin):
  """ USUARIOS """
  queryset = Usuario.objects.all()
  serializer_class = UsuarioModelSerializer

  lookup_field = 'username'

  def get_permissions(self):
    permissions = []
    if self.action in ['logout', 'retrieve']:
      permissions = [IsAuthenticated]
    if self.action == 'retrieve':
      permissions = [EsElMismoUsuario]
    return [p() for p in permissions]

  @action(detail=False, url_path='registro', methods=['POST'])
  def registrar_usuario(self, request):
    """ Registrar un nuevo usuario """
    serializer = CrearUsuarioSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user = serializer.save()
    response = {
      'user': UsuarioModelSerializer(user).data
    }
    return Response(response, status=status.HTTP_201_CREATED)

  @action(detail=False, methods=['POST'])
  def login(self, request):
    """ Login de usuario """
    serializer = LoginSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user, token = serializer.save()
    response = {
      'user': UsuarioModelSerializer(user).data,
      'token': token
    }
    return Response(response, status=status.HTTP_200_OK)

  @action(detail=False, methods=['DELETE'])
  def logout(self, request):
    """ Logout de usuario """
    request.user.auth_token.delete()
    response = {
      "detail": "Logout exitoso"
    }
    return Response(response, status=status.HTTP_200_OK)

  @action(detail=False, url_path='get-user', methods=['GET'])
  def obtener_usuario(self, request):
    """ Obtener usuario """
    user = request.user
    response = {
      'user': UsuarioModelSerializer(user).data,
      'token': user.auth_token.key
    }
    return Response(response, status=status.HTTP_200_OK)