from rest_framework.permissions import BasePermission

class EsElMismoUsuario(BasePermission):
  """ Permiso personalizado para el mismo usuario """
  def has_object_permission(self, request, view, obj):
    # if request.user.username == obj.username:
    #   return True
    # return False

    return obj == request.user