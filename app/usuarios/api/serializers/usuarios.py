from app.usuarios.models.usuarios import Usuario

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from rest_framework.authtoken.models import Token

from django.contrib.auth import authenticate

class UsuarioModelSerializer(serializers.ModelSerializer):
  """ Usuario model serializer """
  class Meta:
    model = Usuario
    fields = (
      'first_name',
      'last_name',
      'username',
      'email',
      'tipo_de_usuario'
    )

class CrearUsuarioSerializer(serializers.Serializer):
  """ Crear usuario serializers """
  email = serializers.EmailField(
    max_length=200,
    required=True,
    validators=[
      UniqueValidator(Usuario.objects.all())
    ]
  )

  first_name = serializers.CharField(
    max_length=50,
    required=True
  )

  last_name = serializers.CharField(
    max_length=50,
    required=True
  )

  username = serializers.CharField(
    max_length=50,
    required=True,
    validators=[
      UniqueValidator(Usuario.objects.all())
    ]
  )

  password = serializers.CharField(
    max_length=30,
    required=True
  )

  tipo_de_usuario = serializers.CharField(
    max_length=15,
    required=True
  )

  def validate(self, data):
    """ VALIDAR DATOS """
    usuarios_validos = ['ADMIN', 'INVITADO']
    if data['tipo_de_usuario'] not in usuarios_validos:
      raise serializers.ValidationError({"error": "Tipo de usuario no valido"})
    return data

  def create(self, data):
    """ CREAR USUARIO """
    email = data['email'].lower()
    del data['email']
    nuevo_usuario = Usuario.objects.create_user(
      **data,
      email = email
    )
    return nuevo_usuario

class LoginSerializer(serializers.Serializer):
  """ Login serializer """
  email = serializers.EmailField(
    max_length=200,
    required = True
  )
  password = serializers.CharField(
    max_length=30,
    required = True
  )

  def validate(self, data):
    """ VALIDAR DATOS """

    # Usuario.objects.get(email=data['email'], password=data['password'])
    user = authenticate(
      email = data['email'].lower(),
      password = data['password']
    )
    if not user:
      raise serializers.ValidationError({"error": "Credenciales incorrectas"})
    self.context['user'] = user
    return data

  def create(self, data):
    """ LOGIN """
    token, created = Token.objects.get_or_create(
      user=self.context['user']
    )
    return self.context['user'], token.key