from django.db import models

# Campos comunes
from utils.models.modelos_comunes import CamposComunes

# ABSTRACT USER

from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser, CamposComunes):
  """ Modelo de usuario """

  email = models.EmailField(
    unique=True,
    max_length=200
  )

  first_name = models.CharField(
    max_length=50
  )

  last_name = models.CharField(
    max_length=50
  )

  TIPO_DE_USUARIO = (
    ('ADMIN', 'Administrador'),
    ('INVITADO', 'Invitado')
  )

  tipo_de_usuario = models.CharField(
    max_length=15,
    choices=TIPO_DE_USUARIO,
    default='INVITADO',
    null=False
  )

  USERNAME_FIELD = 'email' # CAMPO PARA HACER LOGIN
  REQUIRED_FIELDS = [
    'first_name',
    'last_name',
    'username',
  ]

  def __str__(self):
    return self.username

  class Meta:
    db_table = 'usuarios'