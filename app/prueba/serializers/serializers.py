# DJANGO REST FRAMEWORK
from rest_framework import serializers

# MODELOS
from app.prueba.models import ModeloPrueba

class ModeloPruebaSerializers(serializers.ModelSerializer):
  """ Serializer de prueba """
  class Meta:
    """ Class meta """
    model = ModeloPrueba
    fields = (
      'edad', 'email', 'id', 'nombre', 'apellido', 
    )


class NuevoModeloPruebaSerializer(serializers.Serializer):
  """ Serializer """
  nombre = serializers.CharField(
    max_length=100,
    allow_blank = False,
    allow_null = False,
    required = True
  )
  apellido = serializers.CharField(
    max_length=50,
    allow_blank = False,
    allow_null = False,
    required = True
  )
  edad = serializers.IntegerField(
    allow_null = False,
    required = True
  )
  email = serializers.EmailField(
    allow_blank = False,
    allow_null = False,
    required = True
  )

  def create(self, data):
    """ Crear un valor """
    nuevo_modelo = ModeloPrueba.objects.create(
      nombre = "HOLA",
      apellido = data['apellido'],
      edad = data['edad'],
      email = data['email']
    )
    # nuevo_modelo = ModeloPrueba.objects.create(**data)
    return data