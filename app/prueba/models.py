from django.db import models

# Create your models here.

class ModeloPrueba(models.Model):
  """ Modelo de prueba """
  # IMPRE SE CREA UN CAMPO ID
  nombre = models.CharField(max_length=100)
  apellido = models.CharField(max_length=100)
  edad = models.IntegerField()
  email = models.EmailField()

  def __str__(self):
    return self.nombre

  class Meta:
    """ Class meta """
    db_table = 'modelo_prueba'