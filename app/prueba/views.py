# DECORADOR (ESTABLECE EL TIPO DE PETICIÓN)
from rest_framework.decorators import api_view

# ENVIA LA RESPUESTA
from rest_framework.response import Response

# DEFINE EL STATUS DE LA RESPUESTA
from rest_framework import status

# SERIALIZERS
from app.prueba.serializers.serializers import (
  NuevoModeloPruebaSerializer,
  ModeloPruebaSerializers
)

# MODELOS
from app.prueba.models import ModeloPrueba

@api_view(['GET', 'POST'])
def generic_view(request):
  """ VISTA PARA GET Y POST """
  print(request)
  if request.method == 'GET':
    # REALIZO LA CONSULTA
    queryset = ModeloPrueba.objects.all()

    # SERIALIZO LOS DATOS
    serializer = ModeloPruebaSerializers(queryset, many=True)

    # DEVUELVO LA RESPUESTA
    return Response(data = serializer.data, status=status.HTTP_200_OK)
  
  if request.method == 'POST':
    serializer = NuevoModeloPruebaSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    nuevo_modelo = serializer.save()
    return Response(data = nuevo_modelo, status=status.HTTP_201_CREATED)

  # if request.method == 'PUT':
  #   queryset = ModeloPrueba.objects.all()

  #   # SERIALIZO LOS DATOS
  #   serializer = ModeloPruebaSerializers(queryset, many=True)

  #   # DEVUELVO LA RESPUESTA
  #   return Response(data = serializer.data, status=status.HTTP_200_OK)
